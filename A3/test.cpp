#include <iostream>
#include <cmath>
#include <assert.h>
#include <typeinfo>
#include<sstream>
#include<limits>
#include <vector>
using namespace std;

class California
{
private:
    vector<double> values; 
public:
    California() { }
    void read();
    double average();
    //double standard_deviation();
    void show(double *data, size_t count);
    bool testRead(int i);
    bool testAve();
};


void California::read()
{
    double value;
    cout << "Please enter a value: \n";
    cin >> value;

    int i = 0;
    while (i != 10) {
		if(cin.good()){        
		values.push_back(value);}
        i++;

        cout << "Please enter another value: \n";
        cin >> value;
		   
	}
	if(!(this->testRead(i))){
		cout << "test failed invalid input\n";
		return;
	}
	if((this->testRead(i))){
	cout << "Worked properly\n";
       }	
}
bool California::testRead(int i)
{
    return this->values.size() == i;
}
double California::average()
{
    double a;
    double sum = 0;

    for (int i = 0; i < 10; i++)
        sum += values[i];

    a = sum / 10;
    return a;
}

main ()
{
    California cal;

    cal.read();

    cout << "Average = " << cal.average() << endl;
    //cout << "Standard Deviation = " << cal.standard_deviation() << endl;
    
    cout << endl;
    
}
